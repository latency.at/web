const webpack = require('webpack');

const Merge = require('webpack-merge');
const CommonConfig = require('./webpack.config.js');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');

const ImageminPlugin = require('imagemin-webpack-plugin').default;

module.exports = Merge(CommonConfig, { // eslint-disable-line new-cap
  devtool: 'cheap-module-source-map',
  plugins: [
    // Appears to be needed even though we set NODE_ENV outside which again is
    // needed for babel.
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production'),
      },
    }),

    // Copy fonts so we can import them in emails
    new CopyWebpackPlugin([
      {from: 'fonts', to: 'fonts'},
    ]),

    new BabiliPlugin({}, {comments: false}),
    new ImageminPlugin({
      pngquant: {
        quality: '95-100',
      },
    }),
  ],
});

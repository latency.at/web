# React Frontend
- Includes site specific configuration in [config/main.js]
- This probably isn't very idiomatic, I'm not a frontend developer
- `npm start` to run dev server
- See [aws/] for cloudformation template for Cloudfront + S3 setup

## Notes
- Webfont is not open source and maybe not be distributed. See
  [app/fonts](app/fonts) for more details.

## Style Guide
- `expressions on screen or snippets to be copied`: Stuff that is code or
  derrived (like identifiers based on code)
  - eg: `ERROR: foobar`, `curl foo | grep bar`
- *human input*: Stuff a user comes up with
  - eg: *My Super Site*

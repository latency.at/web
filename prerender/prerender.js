const chromeLauncher = require('chrome-launcher');
const chromeInterface = require('chrome-remote-interface');
const fs = require('fs-extra');
const cheerio = require('cheerio');

const url = process.argv[2];
const bundle = JSON.parse(fs.readFileSync('dist/assets.json', 'utf8')).main.js;

async function mirror(path) {
  const ldir = 'dist' + path;
  fs.ensureDirSync(ldir);

  let chrome;
  try {
    chrome = await chromeLauncher.launch({
      chromeFlags: ['--headless', '--disable-gpu', '--no-sandbox'],
    });
    let client = await chromeInterface({port: chrome.port});

    const {Page, Runtime, DOM} = client;
    await Page.enable();
    await Runtime.enable();
    await DOM.enable();
    await Page.navigate({url: url+path+'?prerender=1'}),
    await Page.loadEventFired();

    let response = await Runtime.evaluate({
      expression: 'document.documentElement.outerHTML',
    });

    let docType = await Runtime.evaluate({
      expression: 'document.doctype.name',
    });

    // Add bundled js back
    let dom = cheerio.load(response.result.value);
    dom('body').append(`<script type="text/javascript" src="${bundle}">`);
    fs.writeFileSync(ldir + '/index.html', `<!DOCTYPE ${docType.result.value}>`
      + '\n' + dom.html());
    console.log('written', ldir);
  } catch(e) {
    console.log(e);
  } finally {
    if (chrome) chrome.kill();
  }
}

[
  '/',
  '/configuration',
  '/faq',
  '/legal/terms',
  '/legal/privacy',
  '/legal/imprint',
  '/login',
].forEach((path) => {
  mirror(path);
});

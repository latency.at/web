#!/bin/sh
set -eu
SRC=$1
BUCKET=$2

# aws --region us-east-1 s3 \
#  cp --recursive dist/ "s3://$BUCKET/"

tmp=$(mktemp -d)
cp -r "$SRC/"* "$tmp/"
cd "$tmp"

find . -type f|cut -c 3-|while read f; do
  mime=$(file --brief --mime-type "$f")
  ttl=31536000
  case "$f" in
    index.html|*/index.html|main.js.map)
      ttl=60
      ;;
    fonts/*|favicon.png|main.js.map)
      ttl=3600
      ;;
  esac
  echo "- $f (ttl $ttl)"
  aws --region us-east-1 s3api put-object \
    --content-type "$mime" \
    --cache-control "max-age=$ttl" \
    --bucket "$BUCKET" \
    --key "$f" \
    --body "$f"
done
rm -r "$tmp"

let url = new URL(window.location.href);
const prerender = url.searchParams.get('prerender') == 1;

// imported via resolve
import Pace from 'pace';
if (!prerender) {
  Pace.start();
}

/* eslint-disable no-unused-vars */
import React from 'react';
/* eslint-enable no-unused-vars */
import {render} from 'react-dom';

require('./styles/app.scss');

render(
  <div className="public">
    <div id="home">
      <article className="intro">
        <div className="wrapper wrapper-wide">
          <h1>Latency.at</h1>
        </div>
        <div className="wrapper">
          <p className="text-large">
            <i>Performance and Availability Metrics For Your Monitoring
            &nbsp;System.</i>
            <br />
            * 2017 - 2018 ✝
          </p>
          <div className="button-bar">
            Service was shutdown on July 2018. For contact and inquiry, visit <a href="https://5pi.de/hire-me">5pi.de</a>.
          </div>
        </div>
      </article>
    </div>
  </div>
  , document.getElementById('app'));

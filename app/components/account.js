import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

const Account = (props) => {
  const childs = React.Children.map(props.children, (child) => {
     return React.cloneElement(child, {
       api: props.api,
       notify: props.notify,
     });
	});
  return <div id="account">
    <nav>
      <ul>
        <li><Link activeClassName="active" to="/dashboard/account/password">
          Change Password
        </Link></li>
        <li><Link activeClassName="active" to="/dashboard/account/email">
          Change Email Address
        </Link></li>
      </ul>
    </nav>
    {childs}
  </div>;
};

export default Account;
Account.propTypes = {
  api: PropTypes.object,
  notify: PropTypes.func,
  children: PropTypes.element.isRequired,
};

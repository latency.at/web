Collectd has no native support for Prometheus metrics, but you can still
integrate Latency.at by using the [exec
plugin](https://collectd.org/wiki/index.php/Plugin:Exec) and a script to get the
metrics from Latency.at and format them as appropriate for your setup.

We will provide an official exec plugin soon. Need it now? [Let us
know!](mailto:sales@latency.at).

Latency.at provides all metrics in the [Prometheus Exposition
Format](https://prometheus.io/docs/instrumenting/exposition_formats/), making it
easy to consume by your Prometheus server. We created a Config Generator to get
you started quickly.

#### Quick Start
1. Add targets to generator
2. Configure Settings and Probes
3. Download config via Download button
4. *Edit config to insert your auth, when not already logged in*
5. Run Quick Start Dashboard:

```
docker run -p 3000:3000 -p 9090:9090 \
  -v $PWD/prometheus.yml:/etc/prometheus/prometheus.yml \
  latencyat/dashboard
```

Access Grafana on http://localhost:3000
and Prometheus on http://localhost:9090.

Or run the official Prometheus image, without Grafana dashboards:
```
docker run -p 9090:9090 \
  -v $PWD/prometheus.yml:/etc/prometheus/prometheus.yml \
  prom/prometheus
```

While Nagios isn't really a metrics system, you can use it with Latency.at to
check the availability of your sites and services.

The easiest option is to configure Nagios to execute the
[check_http](https://www.monitoring-plugins.org/doc/man/check_http.html) like
this to check for availability:

```
./check_http -k 'Authorization: Bearer your-token' -S -H nyc1.do.mon.latency.at \
  -u '/probe?module=http_2xx&target=https%3A%2F%2Fapi.latency.at%2F' -s 'probe_success 1'
```
<aside>Please keep in mind: The Performance Data returned by check_http is about
the request to the Probes, not the request the prober does to your site. To
gather that, a custom nagios check would be required.</aside>

This alert will fire when `api.latency.at` isn't reachable anymore.

For more sophisticated checks you can write a [custom
plugin](https://nagios-plugins.org/doc/guidelines.html). We will provide an
official nagios plugin soon. Need it now? [Let us know!](mailto:sales@latency.at).

import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

import Headroom from 'react-headroom';

export default class PublicContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.toggleMenu = this.toggleMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
	}
  toggleMenu(e) {
    this.setState({showMobileNav: !this.state.showMobileNav});
  }
  closeMenu() {
    this.setState({showMobileNav: false});
  }
  render() {
    let childs = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, {
        api: this.props.api,
        notify: this.props.notify,
      });
    });
    const navClass = this.state.showMobileNav ? 'nav-show' : 'nav-hide';

    // Force headroom wrapper to be fixed when nav is active.
    // Prevents some weird flapping
    const wrapperStyle = this.state.showMobileNav && {
      position: 'fixed',
      zIndex: '2000',
      width: '100%',
    };
    return <div className="public">
      <Headroom upTolerance={50} downTolerance={50} wrapperStyle={wrapperStyle}>
        <header>
          <nav className={'navbar ' + navClass}>
            <a onClick={this.toggleMenu} className="abs-tr burger">
              <span className="burger-line"/>
              <span className="burger-line"/>
              <span className="burger-line"/>
            </a>
            <ul className="navbar-brand">
              <li className="navbar-brand">
                <Link to="/">Latency.at</Link>
              </li>
            </ul>
            <ul>
              <li><Link activeClassName="active" to="/docs">
                Documentation</Link></li>
              <li><Link activeClassName="active" to="/faq">
                FAQ</Link></li>
              <li className="abs-tr">
                <ul>
                  <li><Link activeClassName="active" to="/login">
                    Login</Link></li>
                  <li><Link activeClassName="active" to="/signup">
                    Sign Up</Link></li>
                </ul>
              </li>
            </ul>
          </nav>
        </header>
      </Headroom>
      <div onClick={this.closeMenu}>
        { childs }
      </div>
      <footer>
        <nav>
          <ul>
            <li>
              &copy; 2017 Johannes Ziemke. All Right Reserved.
            </li>
            <li><Link activeClassName="active" to="/legal/terms">
              Legal</Link></li>
            <li><Link activeClassName="active" to="/legal/privacy">
              Privacy</Link></li>
            <li><Link activeClassName="active" to="/legal/privacy#cookies">
              Cookies</Link></li>
            <li><Link activeClassName="active" to="/legal/imprint">
              Imprint</Link></li>
            <span itemScope itemType="http://schema.org/Organization">
              <link itemProp="url" href="https://latency.at" />
              <li>
                <a itemProp="sameAs" href="https://twitter.com/latencyat">
                  <span className="icon-twitter" />
                </a>
              </li>
              <li>
                <a itemProp="sameAs" href="https://github.com/latency-at">
                  <span className="icon-github-circled" />
                </a>
              </li>
              <li>
                <a itemProp="sameAs" href="https://blog.latency.at">Blog</a>
              </li>
            </span>
          </ul>
        </nav>
      </footer>
    </div>;
  }
};

PublicContainer.propTypes = {
  api: PropTypes.object,
  children: PropTypes.element.isRequired,
  notify: PropTypes.func,
};
